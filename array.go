package main

import "fmt"

func main(){
    array:=[...]int{13,78,90,17}
	slice := array[:]
	slice1 := array[:]
	slice2 := array[:]
	fmt.Println("Slice: ",slice)
	fmt.Println("Slice1: ",slice1)
	fmt.Println("Slice2: ",slice2)
    
	slice1[2] = 10
	fmt.Println("Slice: ",slice)
	fmt.Println("Slice1: ",slice1)
	fmt.Println("Slice2: ",slice2)

	ism := make(map[string]string)
	fmt.Println(ism)
	ism["Bek"]="Bektosh"
	ism["Oy"]="Oybek"
	ism["dev_op"]="Yusufbek"
	fmt.Println(ism)
	 
	for k, v := range ism {
		fmt.Printf("Key: %v. Value: %v\n",k,v)
	}
}